const {Schema} = require('mongoose')

const Rule = new Schema({

    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic',
        index: true,
        required: true,
    },

    type: {
        type: String,
        required: true,
    },

    value: {
        type: Schema.Types.Mixed,
        required: true,
    },

    priority: {
        type: Number,
        default: 0,
    },

    permission: {
        type: Number,
        default: 0,
    },

})

module.exports = Rule
