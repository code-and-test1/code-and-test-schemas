const {Schema} = require('mongoose')

const Worker = new Schema({

    name: {
        type: String,
        trim: true,
        index: true,
        required: true,
    },

    yaml: {
        type: String,
        required: true,
    },

    status: {
        type: String,
        required: true,
        default: 'down',
        enum: ['down', 'up', 'running'],
    },

    is_deleted: {
        type: Boolean,
        default: false,
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

Worker.index({name: true}, {unique: true})

module.exports = Worker
