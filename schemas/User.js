const {Schema} = require('mongoose')

const User = new Schema({

    extenal_id: {
        type: String,
        index: true,
        unique: true,
        required: true,
    },

    name: {
        type: String,
        trim: true,
        index: true,
        required: true,
    },

    group: {
        type: String,
        trim: true,
        index: true,
        default: 'anonymous',
        required: true,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },
})

module.exports = User
