const {Schema} = require('mongoose')

const Job = new Schema({

    worker: {
        type: Schema.Types.ObjectId,
        ref: 'Worker',
        index: true,
        required: true,
    },

    submit: {
        type: Schema.Types.ObjectId,
        ref: 'Submit',
        index: true,
        required: true,
    },

    status: {
        type: String,
        required: true,
        default: 'process',
        enum: ['process', 'done', 'fail'],
    },

    results: {
        type: [Object],
        default: [],
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

module.exports = Job
