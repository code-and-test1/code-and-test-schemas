const {Schema} = require('mongoose')

const Result = new Schema({
    _id: false,

    score: {
        type: Number,
        default: 0,
    },

    time: {
        type: Number,
        default: 0,
    },

    memory_used: {
        type: Number,
        default: 0,
    },

})

const UserQuiz = new Schema({

    user_topic: {
        type: Schema.Types.ObjectId,
        ref: 'UserTopic',
        index: true,
        required: true,
    },

    quiz: {
        type: Schema.Types.ObjectId,
        ref: 'Quiz',
        index: true,
        required: true,
    },

    result: {
        type: Result,
        default: {},
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },
})

UserQuiz.index({user_topic: true, quiz: true}, {unique: true})

module.exports = UserQuiz
