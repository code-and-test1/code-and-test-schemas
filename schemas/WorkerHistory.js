const {Schema} = require('mongoose')

const WorkerHistory = new Schema({

    worker: {
        type: Schema.Types.ObjectId,
        ref: 'Worker',
        index: true,
        required: true,
    },

    event: {
        type: String,
        trim: true,
    },

    description: {
        type: String,
        trim: true
    },

    value: {
        type: Schema.Types.Mixed,
        default: {}
    },

    created_at: {
        type: Date,
        required: true,
        index: true,
        default: Date.now,
    }

})

module.exports = WorkerHistory
