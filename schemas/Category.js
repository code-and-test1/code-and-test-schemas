const {Schema} = require('mongoose')

const Category = new Schema({

    name: {
        type: String,
        trim: true,
        index: true,
        required: true,
        unique: true,
    },

    description: {
        type: String,
        trim: true,
    },

    slug: {
        type: String,
        trim: true,
        index: true,
        unique: true,
        required: true,
    },

    status: {
        type: String,
        trim: true,
        required: true,
        default: 'active',
        enum: ['active', 'in-active'],
    },

    is_deleted: {
        type: Boolean,
        default: false,
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

// Category.index({name: true}, {unique: true})

module.exports = Category
