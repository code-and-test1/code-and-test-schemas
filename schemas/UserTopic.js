const {Schema} = require('mongoose')

const General = new Schema({
    _id: false,

    permission: {
        type: Number,
        default: 0,
    }
})

const UserTopic = new Schema({

    user: {
        type: String,
        index: true,
        required: true,
    },

    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic',
        index: true,
        required: true,
    },

    general: {
        type: General,
        default: {}
    },

    is_deleted: {
        type: Boolean,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },
})

UserTopic.index({user: true, topic: true}, {unique: true})

module.exports = UserTopic
