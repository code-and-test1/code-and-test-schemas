const {Schema} = require('mongoose')

const Heartbeat = new Schema({

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

module.exports = Heartbeat
