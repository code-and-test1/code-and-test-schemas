const {Schema} = require('mongoose')

const Topic = new Schema({

    name: {
        type: String,
        trim: true,
        index: true,
        required: true,
    },

    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        index: true,
        required: true,
    },

    author: {
        type: String,
        index: true,
        required: true,
    },

    description: {
        type: String,
        trim: true,
    },

    tags: {
        type: [{type: Schema.Types.String, trim: true}],
        index: true,
        default: [],
    },

    status: {
        type: String,
        trim: true,
        required: true,
        default: 'active',
        enum: ['active', 'in-active'],
    },

    password: {
        type: String,
        default: '',
    },

    is_deleted: {
        type: Boolean,
        default: false,
    },

    start_at: {
        type: Date,
        default: Date.now,
    },

    expired_at: {
        type: Date,
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

Topic.index({name: true, category: true}, {unique: true})

module.exports = Topic
