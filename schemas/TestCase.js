const {Schema} = require('mongoose')

const TestCase = new Schema({

    quiz: {
        type: Schema.Types.ObjectId,
        ref: 'Quiz',
        index: true,
        required: true,
    },

    input: {
        type: String,
        default: '',
    },

    output: {
        type: String,
        default: '',
    },

})

module.exports = TestCase
