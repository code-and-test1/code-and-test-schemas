const {Schema} = require('mongoose')

const Stat = new Schema({
    _id: false,

    difficult: {
        type: String,
        required: true,
        default: 'easy',
        enum: ['easy', 'medium', 'hard'],
    },

    author: {
        type: String,
        trim: true,
        index: true,
    },

    submitted: {
        type: Number,
        default: 0,
    },

    score: {
        type: Number,
        default: 100,
    },
})

const Template = new Schema({
    lang: {
        type: String,
    },

    content: {
        type: String,
        trim: true,
    },
})

const Limit = new Schema({
    _id: false,

    submit: {
        type: Number,
        trim: true,
        default: 5,
    },

    memory: {
        type: Number,
    },

    cpu: {
        type: Number,
    },

    timeout: {
        type: Number,
    },
})

const Quiz = new Schema({

    topic: {
        type: Schema.Types.ObjectId,
        ref: 'Topic',
        index: true,
    },

    name: {
        type: String,
        trim: true,
        index: true,
        required: true,
    },

    description: {
        type: String,
        trim: true,
        index: true,
    },

    content: {
        type: String,
        trim: true,
    },

    status: {
        type: String,
        trim: true,
        required: true,
        default: 'active',
        enum: ['active', 'in-active'],
    },

    templates: {
        type: [Template],
        default: [],
    },

    stat: {
        type: Stat,
        default: {},
    },

    limit: {
        type: Limit,
        default: {},
    },

    is_deleted: {
        type: Boolean,
        default: false,
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

Quiz.index({name: true, topic: true}, {unique: true})

module.exports = Quiz
