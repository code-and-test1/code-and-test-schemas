const {Schema} = require('mongoose')

const Result = new Schema({
    _id: false,

    score: {
        type: Number,
        default: 0,
    },

    time: {
        type: Number,
        default: 0,
    },

    memory_used: {
        type: Number,
        default: 0,
    },

})

const Submit = new Schema({

    // user: {
    //     type: String,
    //     default: 'system',
    // },

    user_quiz: {
        type: Schema.Types.ObjectId,
        ref: 'UserQuiz',
        index: true,
        required: true,
    },

    // quiz: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Quiz',
    //     index: true,
    //     required: true,
    // },

    language: {
        type: String,
        trim: true,
        index: true,
        required: true,
        default: 'js',
        enum: ['js', 'py', 'go', 'c', 'cpp'],
    },

    upload_file: {
        type: String,
        required: true,
    },

    status: {
        type: String,
        required: true,
        default: 'pending',
        enum: ['pending', 'processing', 'completed', 'failed', 'retry'],
        index: true,
    },

    result: {
        type: Result,
        default: {},
    },

    updated_at: {
        type: Date,
        default: Date.now,
    },

    created_at: {
        type: Date,
        default: Date.now,
        index: true,
    },

})

module.exports = Submit
